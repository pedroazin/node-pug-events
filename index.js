const path = require('path')
const app = require('express')();
const bodyParser = require('body-parser')
const morgan =  require('morgan')
const methodOverride = require('method-override')

app.use(morgan('dev'))
app.use(bodyParser.urlencoded({extended : false}))
app.use(bodyParser.json())
app.use(methodOverride('_method'))

app.set('view engine', 'pug')
app.set('views', path.join(__dirname, 'src/view'))

app.listen(3000,()=>console.log("Rodando"))

require('./src/model/index')
require('./src/index')(app)

app.get('/', (req,resp)=>{
  return resp.json({"ola":"TESTe"})
})