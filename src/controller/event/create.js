const sequielize = require('../../model/index')
const Event = sequielize.import('./../../model/event')

module.exports = (req, res) =>{
  Event.create(req.body)
    .then(() => res.redirect('/events/'))
  
}