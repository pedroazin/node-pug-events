const sequielize = require('../../model/index')
const Event = sequielize.import('./../../model/event')

module.exports = (req, res) => {
  Event.findById(req.params.id)
    .then((event) => {
      return res.render('event/detail', {
        title: 'Event',
        msg: 'Event',
        event
      })
    })
}