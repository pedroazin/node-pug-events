const sequielize = require('../../model/index')
const Event = sequielize.import('./../../model/event')

module.exports = (req, res) => {
  Event.findAll()
    .then((events) => {
      return res.render('event/index', {
        title: 'All events',
        msg: 'All Events',
        events
      })
    })
}