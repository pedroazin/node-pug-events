const express = require('express')
const router = express.Router()

router.get('/new', require('./new'))
router.get('/', require('./find'))
router.get('/:id', require('./detail'))
router.get('/edit/:id', require('./edit'))
router.post('/', require('./create'))
router.put('/:id', require('./update'))
router.delete('/:id', require('./delete'))

module.exports = router;