const sequielize = require('../../model/index')
const Event = sequielize.import('./../../model/event')

module.exports = (req,res)=>{
  return res.render('event/new', { title: "New Event" , event : new Event() , "msg" : "New Event" })
}