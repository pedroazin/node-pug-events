const sequielize = require('../../model/index')
const Event = sequielize.import('./../../model/event')

module.exports = (req, res) => {
  Event.update(req.body, {
      where: {
        id: req.params.id
      }
    })
    .then(() => res.redirect('/events/'))

}